let flattens=(elements)=>{
      let flat = []
      for(let i=0;i<elements.length;i++){

              if(Array.isArray(elements[i])){
                   
                 flat = flat.concat(flattens(elements[i]))
              }
              else{

                    flat.push(elements[i])
                    
              }
      }

      return flat
    
}
module.exports = flattens
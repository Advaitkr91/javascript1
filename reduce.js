let reduce=(elements,cb,startingValue)=>{
         let index = startingValue >= 0 ? startingValue : 0;
         let current = elements[index]
         for(let i=index+1;i<elements.length;i++){

            current = cb(current,elements[i],i,elements)
         }
        return current 
   }
module.exports = reduce